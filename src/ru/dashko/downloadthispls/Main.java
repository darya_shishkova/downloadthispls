package ru.dashko.downloadthispls;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс для скачивания картинок с веб-страницы
 *
 * @author Дарья "dASHKO" Шишкова, 16ИТ18к
 */
public class Main {

    private static final String IN_FILE_TXT = "src\\ru\\dashko\\downloadthispls\\src\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\dashko\\downloadthispls\\src\\outFile.txt";
    private static final String PATH_TO_PICTURE = "src\\ru\\dashko\\downloadthispls\\downloads\\meme";

    public static void main(String[] args) {
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));) {
            while ((Url = inFile.readLine()) != null) {
                URL url = new URL(Url);
                downloadLinkFinder(url);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        downloadAll();
    }

    /**
     * Скачивает все изображения формата .jpg и .png, ссылки на скачивание которых содержатся на странице
     */
    private static void downloadAll() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String music;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    if(music.contains("http")) {
                        if (music.contains(".jpg")) {
                            downloadUsingNIO(music, PATH_TO_PICTURE + String.valueOf(count) + ".jpg");
                            count++;
                        } else if (music.contains(".png")) {
                            downloadUsingNIO(music, PATH_TO_PICTURE + String.valueOf(count) + ".png");
                            count++;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Ищет ссылки на скачивание файлов (картинок) на веб странице
     * @param url ссылка на страницу
     * @throws IOException ошибки
     */
    private static void downloadLinkFinder(URL url) throws IOException {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            String result;
            try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
                result = bufferedReader.lines().collect(Collectors.joining("\n"));
            }
            Pattern email_pattern = Pattern.compile("\\s*(?<=src\\s?=\\s?\")[^> ]*\\/*(?=\")");
            Matcher matcher = email_pattern.matcher(result);
            int i = 0;
            while (matcher.find() && i < 50) {
                outFile.write(matcher.group() + "\r\n");
                i++;
            }
        }
    }

    /**
     * Скачивает файл
     * @param strUrl ссылка на скачивание
     * @param file путь к будущему файлу
     * @throws IOException ошибки
     */
    private static void downloadUsingNIO(String strUrl, String file) throws IOException {
        URL url = new URL(strUrl);
        ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
        FileOutputStream stream = new FileOutputStream(file);
        stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
        stream.close();
        byteChannel.close();
    }
}